---
title: Plasma Mobile
weight: 7
group: "features"
SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
SPDX-License-Identifier: CC-BY-SA-4.0
---

Plasma Mobile is a mobile platform developed using similar technologies
to Plasma Desktop. To create an application for Plasma Mobile, take a
look at our [Kirigami](../kirigami) tutorial. You can also create
widgets using the [Plasma Components](../plasma).



