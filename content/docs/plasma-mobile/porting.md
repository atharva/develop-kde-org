---
title: Porting a new device to Plasma Mobile
SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
SPDX-License-Identifier: CC-BY-SA-4.0
---

Plasma Mobile currently only supports device running a mainline
Linux kernel. Previously Plasma Mobile also supported Halium
devices (using Android kernel+userspace) but the support was
dropped in favor of [focusing on mainline device](https://www.plasma-mobile.org/2020/12/14/plasma-mobile-technical-debt/).

To port a device to mainline Linux, please refer to [Porting to a new device in postmarketOS wiki](https://wiki.postmarketos.org/wiki/Porting_to_a_new_device).
